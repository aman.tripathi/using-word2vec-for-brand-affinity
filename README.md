The goal of this project is to find similar brands which share the closest cosine similarity.

Overview -  Specifically , this is done by accepting a list of brands from the user. 
The script then recommends the most similar brands for each brand present in the list based on their cosine scores.

Model Type - This model builds a deep neural network at the back end and uses skip-gram and CBOW
to predict the context given a target word or vice-versa.

Usage - To train and get the recommended brands , run this script.